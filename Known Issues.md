# Overview
This document descibes known issues with the current version seperated into section.

# ADC
- Data checking
    - Placeholder input signal ranges
    - Missing logic for detection of sensor connected, disconnected (goes in management loop?)

# Pulse
- Data checking
    - No link with Graseby waveform (stream 1, ADC) for valid signal detection

# Proxytrack
- No calibration
- Uncertain/unknown calibration coefficients
- Data checking
    - Need signal ranges for valid signal (pre or psot calibration?)
    - No detection of sensor present, or invalid data

# Camera
- Potential issue with mkv timestamp correction
- Logger - Error - Code 2 Property Node (Arg :1) in P[rocess Stream Writer...]

# Philips
- Potential issue with ECG composite packet decoding
- wave array flags not being decoded
- TO INVESTIGATE: alert alarm limits
- timestamp mismatch between philips monitor, network time, and time output over serial port
- Calibartion coeffcieints for PLeth waveform give zeros
- Loses connection after some time but never raises an error.
    - now raising an error but seems stuck wherever the error is.

# Indicator module
- 33 for state field occasioanlly?

# IR Driver
- same as indicator module
- needs logic and control loop for setting IR brightness

# Logging
- Philips global variant not being logged
- Detection of mode on startup
- Default state shoudl be logging disabled (if no IND present)
- Multiple files generated whe no change in state, or just change in logging enabled (channel, global, or stream)
- Is the gzip section zipping folders older than 1 day?

# Management
- Detection of overall state and channel state? (channel state could go in channel itself)
    - Detection of infant presence
- Anonymisation
- What should appen to infant iD and bedspace if Philips connection is lost?

# File management


# State_Check.exe
- Improved formatting of cluster and array output, with increaed verbosity.
- Ability to set channel logging state


# Misc
- confiogurable options for network check (global options in config.ini)
- FLickering of Syslog display
- FP in general needs a tidy up
- Standardisation of in-VI documentation
- Overall documentation of class structure
- Front panel locked during high intsensity of events (syslog updates?)
- all acquisiiton channels need state to be updated periodically to reset pdate timer counter
    - overhaul of the entire syslog/state checking system.
        - Proposed changes:
            - seperation of syslog/status messages and more generic state structure
            - state structure:
                - chanel state: OK, ERROR, WARNING, DISABLED
                - channel stage: CREATION, INIT,..., CLOSE, DISCARD
                - 1D array of cluster of 2 elements:
                    - stream state? (same type as channel state)
                    - data validity - cluster of 3:
                        - signal
                        - device
                        - system