﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Channel Data" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Channel.lvlib:Plotter.lvlib:Channel Data.ctl</Property>
		<Property Name="typedefName2" Type="Str">Channel.lvlib:Queue Data.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../Channel/Plotter/_TypeDef/Channel Data.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">../Channel/_TypeDef/Queue Data.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%3$A%!!"E!A!!!!!!,!""!-0````](1WBB&lt;GZF&lt;!!11$$`````"V.U=G6B&lt;8-!&amp;%"!!!(`````!!%(5X2S:7&amp;N=Q!'!&amp;1!"A!91%!!!@````]!!QJ5;7VF=X2B&lt;8"T!!!&amp;!!I!!"*!1!!"`````Q!&amp;"%2B&gt;'%!!"J!5!!#!!1!"AZP&gt;82Q&gt;81A9WRV=X2F=A!!'%"!!!(`````!!=,5X2S:7&amp;N)%2B&gt;'%!&amp;%"1!!-!!!!#!!A(1WRV=X2F=A",!0%!!!!!!!!!!QV$;'&amp;O&lt;G6M,GRW&lt;'FC$6"M&lt;X2U:8)O&lt;(:M;7)11WBB&lt;GZF&lt;#"%982B,G.U&lt;!!71%!!!@````]!#16"=H*B?1!"!!I!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Globals" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">12</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Channel.lvlib:Channel State.ctl</Property>
		<Property Name="typedefName10" Type="Str">Globals_Logging.ctl</Property>
		<Property Name="typedefName11" Type="Str">Globals_State.ctl</Property>
		<Property Name="typedefName12" Type="Str">Globals_System.ctl</Property>
		<Property Name="typedefName2" Type="Str">Channel.lvlib:Philips.lvlib:Philips_Monitor_Mode.ctl</Property>
		<Property Name="typedefName3" Type="Str">Channel.lvlib:Syslog Severity Codes.ctl</Property>
		<Property Name="typedefName4" Type="Str">Globals.ctl</Property>
		<Property Name="typedefName5" Type="Str">Globals_Config.ctl</Property>
		<Property Name="typedefName6" Type="Str">Globals_Demographics.ctl</Property>
		<Property Name="typedefName7" Type="Str">Globals_Indicator.ctl</Property>
		<Property Name="typedefName8" Type="Str">Globals_IO.ctl</Property>
		<Property Name="typedefName9" Type="Str">Globals_IRDriver.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../Channel/_TypeDef/Channel State.ctl</Property>
		<Property Name="typedefPath10" Type="PathRel">../Utility/Variables/Globals_Logging.ctl</Property>
		<Property Name="typedefPath11" Type="PathRel">../Utility/Variables/Globals_State.ctl</Property>
		<Property Name="typedefPath12" Type="PathRel">../Utility/Variables/Globals_System.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">../Channel/Philips/_TypeDef/Philips_Monitor_Mode.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">../Channel/_TypeDef/Syslog Severity Codes.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">../Utility/Variables/Globals.ctl</Property>
		<Property Name="typedefPath5" Type="PathRel">../Utility/Variables/Globals_Config.ctl</Property>
		<Property Name="typedefPath6" Type="PathRel">../Utility/Variables/Globals_Demographics.ctl</Property>
		<Property Name="typedefPath7" Type="PathRel">../Utility/Variables/Globals_Indicator.ctl</Property>
		<Property Name="typedefPath8" Type="PathRel">../Utility/Variables/Globals_IO.ctl</Property>
		<Property Name="typedefPath9" Type="PathRel">../Utility/Variables/Globals_IRDriver.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!^P;Q]!!"E!A!!!!!"U!"2!-0````],5G6N&lt;X2F)%F18T%!&amp;%!Q`````QN3:7VP&gt;'5A36"@-A!?1$$`````&amp;%:J&lt;'6O97VF)%:P=GVB&gt;#"4='6D!!!D1!)!(%2F:G&amp;V&lt;(1A5X2S:7&amp;N)%*V:G:F=C"-:7ZH&gt;'A!!"*!-P````]*9G&amp;T:3"Q982I!(E!]1!!!!!!!!!#$5.I97ZO:7QO&lt;(:M;7):5XFT&lt;'^H)&amp;.F&gt;G6S;82Z)%.P:'6T,G.U&lt;!"*1"9!#!6F&lt;76S:Q6B&lt;'6S&gt;!2D=GFU!W6S=A2X98*O"GZP&gt;'FD:12J&lt;G:P"72F9H6H!"".;7YA4'6W:7QA+'2J=X!J!!"X!0%!!!!!!!!!!AV$;'&amp;O&lt;G6M,GRW&lt;'FC'6.Z=WRP:S"4:8:F=GFU?3"$&lt;W2F=SZD&gt;'Q!2U!7!!A&amp;:7VF=G=&amp;97RF=H1%9X*J&gt;!.F=H)%&gt;W&amp;S&lt;A:O&lt;X2J9W5%;7ZG&lt;Q6E:7*V:Q!047FO)%RF&gt;G6M)#BM&lt;W=J!$M!]&gt;TNH*)!!!!"%E&gt;M&lt;W*B&lt;(.@1W^O:GFH,G.U&lt;!!A1&amp;!!"Q!!!!%!!A!$!!1!"1!'"E.P&lt;G:J:Q!!$E!Q`````Q21&lt;X*U!!!51$$`````#V*F=W6S&gt;G6E)%*Z!!R!6!!'"&amp;2J&lt;75!!"J!5!!$!!A!#1!+$6*F=W6S&gt;G6E)&amp;"P=H1!)%"!!!(`````!!M35G6T:8*W:71A1U^.)("P=H2T!!!H!0(=UD0=!!!!!1Z(&lt;'^C97RT8UF0,G.U&lt;!!11&amp;!!!1!-!UEP4Q!11$$`````"EFO:G&amp;O&gt;!!!%U!'!!VQ982@:'VH8X.U982F!!^!"A!)='&amp;U8X2Z='5!!"6!"A!/='&amp;U8X"B9W6E8WVP:'5!!"2!-0````]+:WFW:7Z@&lt;G&amp;N:1!!&amp;%!Q`````QNG97VJ&lt;(F@&lt;G&amp;N:1!11$$`````"H"B&gt;&amp;^J:!!!$5!'!!&gt;Q982@=W6Y!!V!"1!(9W6O&gt;(6S?1!,1!5!"(FF98)!!!N!"1!&amp;&lt;7^O&gt;'A!#5!&amp;!!.E98E!#U!&amp;!!2I&lt;X6S!!!.1!5!"GVJ&lt;H6U:1!!$5!&amp;!!:T:7.P&lt;G1!!!^!"1!):H*B9X2J&lt;WY!!"Z!5!!)!"9!&amp;Q!9!"E!'A!&lt;!"Q!(1&gt;Q982@:'^C!!N!"A!&amp;&gt;7ZJ&gt;(-!%E!Q`````QFV&lt;GFU=V^T&gt;()!#U!+!!6W97RV:1!91&amp;!!!Q!@!#!!)1JQ982@;'6J:WBU!!!91&amp;!!!Q!@!#!!)1JQ982@&gt;W6J:WBU!!!51&amp;!!!Q!@!#!!)1&gt;Q982@97&gt;F!$*!5!!,!!]!%!!2!")!%Q!5!"5!(A!C!#-!*"21;'FM;8"T)%2F&lt;7^H=G&amp;Q;'FD=Q!!01$RX)BKI1!!!!%92WRP9G&amp;M=V^%:7VP:X*B='BJ9X-O9X2M!"R!5!!#!!Y!*1R%:7VP:X*B='BJ9X-!!"J!-0````]15W^G&gt;(&gt;B=G5A6G6S=WFP&lt;A!!'%!Q`````QZ49WBF&lt;7%A&gt;G6S=WFP&lt;A!!&amp;E!Q`````QR3&gt;7ZU;7VF)%VP:'5!!"*!-0````])3'^T&gt;'ZB&lt;75!!""!-0````]'=XFT8WFE!!!71$$`````$'VB&lt;H6G97.U&gt;8*F=A!!&amp;E!Q`````QRN&lt;W2F&lt;&amp;^O&gt;7VC:8)!!"2!5!!#!#Q!,1FT?8.@&lt;7^E:7Q!%5!(!!NO&lt;WV@&gt;G6S=WFP&lt;A!&lt;1!=!&amp;82F?(2@9W&amp;U97RP:V^S:8:J=WFP&lt;A!01!9!#'RB&lt;G&gt;V97&gt;F!!!.1!9!"G:P=GVB&gt;!!!&amp;E"1!!-!-!!R!$)*=XFT8WRP9W&amp;M!".!"A!-=XFT8W&amp;T=W^D8WFE!!!21!9!#GVE=V^T&gt;'&amp;U&gt;8-!!"*!-0````]*9G6E8WRB9G6M!!V!"A!(&lt;X"@&lt;7^E:1!01!9!#'&amp;Q=&amp;^B=G6B!!!01!9!#8.Q:7.@&gt;(FQ:1!41!9!$'.P&lt;8"P&lt;G6O&gt;&amp;^J:!!!%E!Q`````QFQ=G^E8X.Q:7-!%%"1!!-!/1![!$M$&gt;G&amp;M!":!1!!"`````Q!]#(.Z=V^T='6D!!!K1&amp;!!#A!L!#Y!,Q!T!$1!.1!W!$=!/!!^$F"I;7RJ=(-A5XFT&gt;'6N!!!-1$$`````!U*F:!$/!0%!!!!!!!!!!QV$;'&amp;O&lt;G6M,GRW&lt;'FC$6"I;7RJ=(-O&lt;(:M;7)95'BJ&lt;'FQ=V^.&lt;WZJ&gt;'^S8UVP:'5O9X2M!*&amp;!&amp;A!)#V6O=X"F9WFG;76E&amp;66O=X"F9WFG;76E)#UA5X2B&lt;G2C?12%:7VP$E2F&lt;7]A,3"4&gt;'&amp;O:'*Z#EVP&lt;GFU&lt;X*J&lt;G=547^O;82P=GFO:S!N)&amp;.U97ZE9HE(5W6S&gt;GFD:2&amp;4:8*W;7.F)#UA5X2B&lt;G2C?1!55'BJ&lt;'FQ=S".&lt;WZJ&gt;'^S)%VP:'5!!$M!]&gt;UHJ_E!!!!"%E&gt;M&lt;W*B&lt;(.@5XFT&gt;'6N,G.U&lt;!!A1&amp;!!"Q!H!#A!+1!K!$Y!0Q"!"F.Z=X2F&lt;1!!(%!B&amp;U6O97*M:3"-&lt;W&gt;H;7ZH)#B(&lt;'^C97QJ!"R!)2&gt;&amp;&lt;G&amp;C&lt;'5A4'^H:WFO:S"0&gt;G6S=GFE:1!71#%227ZB9GRF)&amp;"M&lt;X2U;7ZH)$)!*E"!!!(`````!%1927ZB9GRF)%RP:W&gt;J&lt;G=A+%.I97ZO:7QJ!!!%!#%!%%"!!!(`````!%9$152$!"*!1!!"`````Q"'"6"V&lt;(.F!"B!1!!"`````Q"'#F"S&lt;XBZ&gt;(*B9WM!!"2!1!!"`````Q"'"V"I;7RJ=(-!%%"!!!(`````!%9$1U&amp;.!"2!1!!"`````Q"'"F.Z=WRP:Q!!&amp;E"!!!(`````!%9*36)A2(*J&gt;G6S!#R!5!!(!%=!3!"*!%I!3Q"-!%U827ZB9GRF)%RP:W&gt;J&lt;G=A+(.U=G6B&lt;3E!&amp;%!S`````QJ';7RF=S"1982I!!!51$,`````#UFN97&gt;F=S"1982I!"2!-P````]+5X2V:(EA5'&amp;U;!!!0!$RX/Y2WA!!!!%42WRP9G&amp;M=V^-&lt;W&gt;H;7ZH,G.U&lt;!!A1&amp;!!"Q"#!%-!21"/!%]!5!"2"URP:W&gt;J&lt;G=!7Q$R!!!!!!!!!!).1WBB&lt;GZF&lt;#ZM&gt;GRJ9B&amp;$;'&amp;O&lt;G6M)&amp;.U982F,G.U&lt;!!T1"9!"!6&amp;5F*05A&gt;816*/35Z(!E^,#%2*5U&amp;#4%6%!!R4?8.U:7UA5X2B&gt;'5!!&amp;M!]1!!!!!!!!!#$5.I97ZO:7QO&lt;(:M;7)21WBB&lt;GZF&lt;#"4&gt;'&amp;U:3ZD&gt;'Q!-U!7!!1&amp;26*34V)(6U&amp;34EF/2Q*03QB%36."1ER&amp;2!!.4G6U&gt;W^S;S"4&gt;'&amp;U:1!:1!)!%G2F&gt;GFD:6^B&lt;'6S&gt;&amp;^T&gt;'&amp;U:1!!(%"1!!%!62.1;'FM;8"T)%&amp;M:8*U)&amp;.U982F!"N!#A!52H*F:3"4='&amp;D:3!I*3"5&lt;X2B&lt;#E!!"6!#A!02H*F:3"4='&amp;D:3!I2U)J!&amp;M!]1!!!!!!!!!#$5.I97ZO:7QO&lt;(:M;7)21WBB&lt;GZF&lt;#"4&gt;'&amp;U:3ZD&gt;'Q!-U!7!!1&amp;26*34V)(6U&amp;34EF/2Q*03QB%36."1ER&amp;2!!.4'^H:WFO:S"4&gt;'&amp;U:1"&lt;!0%!!!!!!!!!!AV$;'&amp;O&lt;G6M,GRW&lt;'FC%5.I97ZO:7QA5X2B&gt;'5O9X2M!$.!&amp;A!%"5635E^3"V&gt;"5EZ*4E=#4UM)2%F415*-251!$5&gt;S98.F9HEA5X2B&gt;'5!71$R!!!!!!!!!!).1WBB&lt;GZF&lt;#ZM&gt;GRJ9B&amp;$;'&amp;O&lt;G6M)&amp;.U982F,G.U&lt;!!R1"9!"!6&amp;5F*05A&gt;816*/35Z(!E^,#%2*5U&amp;#4%6%!!J';5]S)&amp;.U982F!!":!0%!!!!!!!!!!AV$;'&amp;O&lt;G6M,GRW&lt;'FC%5.I97ZO:7QA5X2B&gt;'5O9X2M!$&amp;!&amp;A!%"5635E^3"V&gt;"5EZ*4E=#4UM)2%F415*-251!#V"S&lt;XBZ)&amp;.U982F!&amp;M!]1!!!!!!!!!#$5.I97ZO:7QO&lt;(:M;7)21WBB&lt;GZF&lt;#"4&gt;'&amp;U:3ZD&gt;'Q!-U!7!!1&amp;26*34V)(6U&amp;34EF/2Q*03QB%36."1ER&amp;2!!.5'BJ&lt;'FQ=S"4&gt;'&amp;U:1"8!0%!!!!!!!!!!AV$;'&amp;O&lt;G6M,GRW&lt;'FC%5.I97ZO:7QA5X2B&gt;'5O9X2M!#^!&amp;A!%"5635E^3"V&gt;"5EZ*4E=#4UM)2%F415*-251!#5.B&lt;3"4&gt;'&amp;U:1!61!1!$V&gt;P=GNJ&lt;G=A5W6U)#B#+1"#!0(&gt;5T;Q!!!!!2&amp;(&lt;'^C97RT8V.U982F,G.U&lt;!!I1&amp;!!$!"4!&amp;1!6A"8!&amp;A!71";!&amp;M!8!"&gt;!&amp;Y!8Q64&gt;'&amp;U:1!-1#%(1U&amp;.8V"85A!.1!5!"UR&amp;2&amp;^16UU!%5!&amp;!!N-4V&gt;&amp;5F^-35V*6!!21!5!#V615%638UR*45F5!"Z!5!!%!'%!9A"D!'1/36)A2(*J&gt;G6S)%2"6%%!!"N!&amp;A!#"5635E^3!E^,!!!*2%678V.5162&amp;!""!-0````](6E635UF04A!21!5!#UR*2UB58UR&amp;6E6-!#2!5!!(!'9!:Q"B!')!9Q"E!'A036)A2(*J&gt;G6S)&amp;.5162&amp;!!R!)1&gt;"&gt;82P)%F3!$=!]&gt;UF5N)!!!!"&amp;%&gt;M&lt;W*B&lt;(.@36*%=GFW:8)O9X2M!"J!5!!$!'5!;1"K#5F3)%2S;8:F=A!.1!)!"UR&amp;2&amp;^16UU!%E"!!!(`````!'Q&amp;5V2"6%5!'%"1!!%!&lt;1Z*&lt;G2J9W&amp;U&lt;X)A2%&amp;511!!%5!#!!N-4V&gt;&amp;5F^-35V*6!!51%!!!@````]!&lt;Q&gt;#8V.5162&amp;!"Z!5!!%!'9!:Q"N!(!037ZE;7.B&gt;'^S)&amp;.5162&amp;!$9!]&gt;UF5M)!!!!"&amp;5&gt;M&lt;W*B&lt;(.@37ZE;7.B&gt;'^S,G.U&lt;!!91&amp;!!!A"O!(%*37ZE;7.B&gt;'^S!$9!]&gt;T3.$=!!!!"#U&gt;M&lt;W*B&lt;(-O9X2M!#*!5!!)!!=!$1!G!%%!5A"A!'M!=A&gt;(&lt;'^C97RT!!%!=Q!!!!!!!!!!!!!!!!!!5&amp;2)-!!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&amp;"53$!!!!!%!!!!!&amp;"53$!!!!!%!!!!!&amp;"53$!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Image Data.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../Utility/Variables/Image Data.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"96!!!!"E!A!!!!!!#!!5!"Q!!.Q$R!!!!!!!!!!%/37VB:W5A2'&amp;U93ZD&gt;'Q!)%"!!!,``````````Q!!#EFN97&gt;F)%2B&gt;'%!!!%!!1!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Indicated States" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Indicated States.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../Utility/Variables/Indicated States.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#/CA!!!"E!A!!!!!!%!"2!-0````]+5X2B&gt;'5A4G&amp;N:1!!%5!(!!N4&gt;'&amp;U:3"797RV:1!31&amp;!!!A!!!!%(1WRV=X2F=A!`!0%!!!!!!!!!!22*&lt;G2J9W&amp;U:71A5X2B&gt;'6T,G.U&lt;!!C1%!!!@````]!!B"*&lt;G2J9W&amp;U&lt;X)A5X2B&gt;'6T!!!"!!-!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="States" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Channel.lvlib:Channel Stage.ctl</Property>
		<Property Name="typedefName2" Type="Str">Channel.lvlib:Data State.ctl</Property>
		<Property Name="typedefName3" Type="Str">Channel.lvlib:Execution State.ctl</Property>
		<Property Name="typedefName4" Type="Str">States.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../Channel/_TypeDef/Channel Stage.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">../Channel/_TypeDef/Data State.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">../Channel/_TypeDef/Execution State.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">../Utility/Variables/States.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(U]!%!!"E!A!!!!!!+!":!-0````]-1WBB&lt;GZF&lt;#"/97VF!!"T!0%!!!!!!!!!!AV$;'&amp;O&lt;G6M,GRW&lt;'FC%5.I97ZO:7QA5X2B:W5O9X2M!%N!&amp;A!)"E.S:7&amp;U:1J*&lt;GFU;7&amp;M;8JF"U&amp;D=86J=G5$4'^H"5.M&lt;X.F"U2J=W.B=G1'5X2B&gt;(6T"5.I:7.L!!!&amp;5X2B:W5!61$R!!!!!!!!!!).1WBB&lt;GZF&lt;#ZM&gt;GRJ9B.&amp;?'6D&gt;82J&lt;WYA5X2B&gt;'5O9X2M!#N!&amp;A!%!E^,"5635E^3#%2*5U&amp;#4%6%"V6/3UZ06UY!"6.U982F!&amp;Y!]1!!!!!!!!!#$5.I97ZO:7QO&lt;(:M;7)/2'&amp;U93"4&gt;'&amp;U:3ZD&gt;'Q!/5!7!!1(35Z715R*2!6715R*2!./,U%035Z715R*2#!N)%635E^3!!J%982B)&amp;.U982F!!!?1%!!!@````]!!R&amp;4&gt;(*F97UA2'&amp;U93"4&gt;'&amp;U:1!91$$`````$URB=X1A5XFT&lt;'^H)%VT:Q!:1!-!%S-A:7RF&lt;76O&gt;(-A;7YA=86F&gt;75!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!&amp;!"1!!=!!!!"!!)!"!!&amp;!!9!"Q!L!0%!!!!!!!!!!1J4&gt;'&amp;U:8-O9X2M!"B!1!!"`````Q!)"F.U982F=Q!!!1!*!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
