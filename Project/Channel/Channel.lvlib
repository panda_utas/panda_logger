﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="_subVI" Type="Folder"/>
	<Item Name="_TypeDef" Type="Folder">
		<Item Name="Channel Stage.ctl" Type="VI" URL="../_TypeDef/Channel Stage.ctl"/>
		<Item Name="Channel State.ctl" Type="VI" URL="../_TypeDef/Channel State.ctl"/>
		<Item Name="Data State.ctl" Type="VI" URL="../_TypeDef/Data State.ctl"/>
		<Item Name="Execution State.ctl" Type="VI" URL="../_TypeDef/Execution State.ctl"/>
		<Item Name="Logging Msg.ctl" Type="VI" URL="../_TypeDef/Logging Msg.ctl"/>
		<Item Name="Options.ctl" Type="VI" URL="../_TypeDef/Options.ctl"/>
		<Item Name="Queue Data.ctl" Type="VI" URL="../_TypeDef/Queue Data.ctl"/>
		<Item Name="State Msg.ctl" Type="VI" URL="../_TypeDef/State Msg.ctl"/>
		<Item Name="State Msg2.ctl" Type="VI" URL="../_TypeDef/State Msg2.ctl"/>
		<Item Name="Syslog Severity Codes.ctl" Type="VI" URL="../_TypeDef/Syslog Severity Codes.ctl"/>
		<Item Name="Syslog.ctl" Type="VI" URL="../_TypeDef/Syslog.ctl"/>
	</Item>
	<Item Name="ADC.lvlib" Type="Library" URL="../ADC/ADC.lvlib"/>
	<Item Name="Camera.lvlib" Type="Library" URL="../Camera/Camera.lvlib"/>
	<Item Name="Channel.lvclass" Type="LVClass" URL="../Channel.lvclass"/>
	<Item Name="File Manager.lvlib" Type="Library" URL="../File Manager/File Manager.lvlib"/>
	<Item Name="Logger.lvlib" Type="Library" URL="../Logger/Logger.lvlib"/>
	<Item Name="Management.lvlib" Type="Library" URL="../Management/Management.lvlib"/>
	<Item Name="Output.lvlib" Type="Library" URL="../Output/Output.lvlib"/>
	<Item Name="Philips.lvlib" Type="Library" URL="../Philips/Philips.lvlib"/>
	<Item Name="Plotter.lvlib" Type="Library" URL="../Plotter/Plotter.lvlib"/>
	<Item Name="Proxytrack.lvlib" Type="Library" URL="../Proxytrack/Proxytrack.lvlib"/>
	<Item Name="Pulse.lvlib" Type="Library" URL="../Pulse/Pulse.lvlib"/>
	<Item Name="Syslog Interpreter.lvlib" Type="Library" URL="../Syslog Interpreter/Syslog Interpreter.lvlib"/>
</Library>
