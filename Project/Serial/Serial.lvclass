﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="19008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Serial.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../Serial.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"&gt;$5F.31QU+!!.-6E.$4%*76Q!!%J!!!!3T!!!!)!!!%H!!!!!A!!!!!AR4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!!!#A'1#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!*&amp;6C:ND$=:+G/U1-Q6&gt;L95!!!!-!!!!%!!!!!$7B?,Y_2ZL2[&lt;=?,:.:-0^V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!+)KN[(FW$EK&gt;?VB2:U*BUA%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$OW(X(X]=\?!CB(76LEJ^R!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!*A!!!#:YH'0A:'"K9,D!!-3-1-T5Q01$S0Y!YD-)=%"F'!$(^!LR!!!!!!")!!!"'(C=9W$!"0_"!%AR-D#Q#!"J&amp;D2R-!VD5R0A-B?886"R:KA&lt;73'CD%!RZB&gt;!"B.)$KK'!S,(QI(E(4D!&amp;'&amp;A!!#8K#9/!!!!$!!"6EF%5Q!!!!!!!Q!!!85!!!-U?*RL9'2A++]Q-R&amp;A9G"A"L)F'2I9EP.45LE9A(Q'"""!ZJ!"'K$G;;'*'RYYH!9%?PTS,7#_2`-&lt;$=`G(USFABY\9'L_8`"I0K*RW+/\%32UX#%"L,#&lt;U@#3K_'"`R-OA,5"&amp;@$,/Y$V.V2;-Z3Q'RY!#RZP`-))-1&lt;$1*B[H]Y40JW(0$IH*L!!685W:A!JTVUQR5!\8!V0?H2?^/A]Z0D`EO(*VA0]5QZ[&gt;*[!/!KIX_0A)R:0O/&amp;?P3R-8MEC4"[&gt;,FQ'RVWY!M2!&gt;P?+=''V0QRIA)*(ZTG%Z#Z'C)JO0I392[&gt;D!!J0!ZH8?!(&amp;&gt;U#"$_A#$S"BJ""X'(]U-;R^@7]8U0M-&lt;%BC$AS1_'.A1M6[$)Q-)-?#S&amp;SI7BMAGQEK*A-6!\(@1NF#3(JG1=5UE-3Y'"&amp;WQM2/A&gt;6!X-!+&amp;:.DB0""&lt;"UA/Q$+NA+S$U$:XE#W!*1&gt;QQAVE!FK+1\;W&gt;`&amp;&amp;3['"*$40Q!2=HQ0!!!!!!!!$BE"A!5!!!9R/3YQ,D%!!!!!!!!-'1#!!!!!"$%Z,D!!!!!!$BE"A!5!!!9R/3YQ,D%!!!!!!!!-'1#!!!!!"$%Z,D!!!!!!$BE"A!5!!!9R/3YQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!"Y?!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!"YL+KLL(A!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!"YL+KDI[/DK[RY!!!!!!!!!!!!!!!!!!!!!0``!!"YL+KDI[/DI[/DI[OM?!!!!!!!!!!!!!!!!!!!``]!K[KDI[/DI[/DI[/DI[/LL!!!!!!!!!!!!!!!!!$``Q#KKK/DI[/DI[/DI[/DI`[L!!!!!!!!!!!!!!!!!0``!+KLK[KDI[/DI[/DI`\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OKI[/DI`\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLKKT_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OL`P\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLK`\_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OL`P\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#LK[OLK[OLK`\_`P\_`KOL!!!!!!!!!!!!!!!!!0``!!#EKKOLK[OL`P\_`KOMJ!!!!!!!!!!!!!!!!!!!``]!!!!!J+OLK[P_`KOLJ!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!+3LK[OLIQ!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!#EIQ!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!!!1!!!!!!!!!K2E!A!!!!!!"!!1!!!!"!!!!!!!$!!!!#W.M98.T5X2S;7ZH'1#!!!!!!!%!#!!Q`````Q!"!!!!!!!30SI[/CB*4F.55HR44U.,261J!!!!!!!!!!VE;8.Q&lt;'&amp;Z2GFM&gt;'6S'1#!!!!!!!%!"1!(!!!"!!!!!3!!!!!!!!!!!!FU?8"F1WRB=X-:!)!!!!!!!1!)!$$`````!!%!!!!!!!6*&lt;H.U=A!!!!!!!!!!!!/8!!!(7XC=L66,4".2&amp;,VPG/)LF0C+)N2)/J!:*&amp;531[)A@F!:4&amp;##V)K`"6I&gt;&amp;".C45O.+T`*R!1.'V%7*OY)C3M8B,AVJH%T]&lt;032"&gt;6NCZUYS@+&gt;,RPJD04)KE&lt;:T&amp;ZG&gt;RTTXXHH0='9-U*VC15Y,I*B(X$R;!*.:J"!()&gt;&amp;)J0WWVA)_1XE0529M)_/M)_#!7SS931:L42&lt;?I5@-6K[[.V$RY1QDZB;47,9,-;%^:K2G0YE*RH]J..]F4!\2K':D:.#M**/@K$4OMJ*!3^H&lt;`$(;1!2'U22&lt;VV/(FZ4*@ZVW!(D&gt;ANAS9QV;B,S`H.W"'JH^MNS3D-E:4&lt;%L"F/SQM,0CAM!.KM]@IZJAZ&lt;,N)2CNA[F7D/30HN^K9'BO$0)MODRINT0$:/7AF&gt;*VK-)1C\JK\Z4G83YW;_WT=UN)3YP"&gt;R)W&lt;M&amp;\/([!2_EF\([C,[U_"!-G&gt;I&gt;9\[T((BY?Y#X:N02KB^"#7Q(8#B*BO#'&gt;"&gt;,H[9-;W182NW)UWE)/W$5&amp;VCI(PQ]Q`@&amp;$&amp;8'NTXU1W-TG7FF)8J0-4S5R'OJ+_&gt;$5Z/3:JS=HEXQ\N59VA&amp;^]^*\0$!1UA1C_-F[K&gt;APHZ?21!XTZU,U)&lt;Z&lt;S(#^O\G@!5VXT&amp;/;OP8#]KJ`Y]&gt;J'LJ`1)Z:HN^T0&lt;C:E.MZ6DF76W_``0\![-UP56G15.9J#NE,]O"V33W2CC&gt;I&amp;7!&gt;/.G"NFG9UBTS[8JV*G&gt;`[&gt;W:D,Z76W&gt;H;W$)@+^XC:&amp;1FR-KO`M:;N::\=8^:H[):&lt;*=E&gt;14N51_)2N,YA2Q1ZO0R6LPS(6]UK1;^/6&gt;:@KA9HLX8("R,\J@29*J6.HS_[Q22H!KQ%O:XAQ)JHU(U4"P(3IUT.2_._.*T^]^E#&gt;FGN#5-9TUYZ@]2*NOWD!(&amp;9"_==6=*2L"9YC[XV85Z18._BE7#U30D!B#/;)@&amp;7N37(2)";&lt;$8M\P*\O60/,P57O)EZE`G89)@]L+&lt;9-G4#-.ZRW(',IVP)[3B!013,-HH$65GZK/-QAT7[R"&gt;[C[V3;X(;,'VV:F_V&amp;#T,+C`VNI:"CK]YQ`RJ]&amp;0\9^5!9J#/9J#55[#="P6H;.EZS&amp;6NN[P[2[6'B9C+,#IHB-$,Q#M@-F)#K9M\%"(00AYU6(*+[)=87?^X2B/S;F5K90X9`)9*$7J@`7MWA++&amp;[!$N:Y`Q"M(,:!.^2,`FXHI`P.SAB^4@1)*MF"^';&gt;-@\^Y*3A!!!!!%!!!!,!!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!!1!!!!!!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!L)!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!'Q:!)!!!!!!!1!)!$$`````!!%!!!!!!&amp;!!!!!#!$"!=!!/"5FO=X2S!!!:!)!!!!!!!1!%!!!!!1!!!!!!!!V736.")(*F=W^V=G.F!"B!5!!"!!!/5W6S;7&amp;M,GRW9WRB=X-!!!%!!1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!J'1#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2E!A!!!!!!"!!5!"Q!!!1!!X8FD)Q!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:'1#!!!!!!!%!"1!(!!!"!!$&gt;?7-D!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!'Q:!)!!!!!!!1!)!$$`````!!%!!!!!!&amp;!!!!!#!$"!=!!/"5FO=X2S!!!:!)!!!!!!!1!%!!!!!1!!!!!!!!V736.")(*F=W^V=G.F!"B!5!!"!!!/5W6S;7&amp;M,GRW9WRB=X-!!!%!!1!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2E!A!!!!!!"!!5!!Q!!!1!!!!!!#!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!"='1#!!!!!!!)!-%"Q!!Y&amp;37ZT&gt;()!!"E!A!!!!!!"!!1!!!!"!!!!!!!!$6:*5U%A=G6T&lt;X6S9W5!'%"1!!%!!!Z4:8*J97QO&lt;(:D&lt;'&amp;T=Q!!!1!"!!!!!!!!!!!!!!!!!!1!"!!,!!!!"!!!!&amp;5!!!#!!!!!!1!!!!9!!!!"X8F@WA!!!!!!!!!!!!!!!!!!!!,&gt;?7-@!!!!#!!!!!!!!!!!!!!!!NVZFTQ!!!!1!!!!!!!!!!!!!!!#X8GUHA!!!"A!!!!!!!!!!!!!!!,&gt;?&lt;7\!!!!)!!!!!!!!!!!!!!!!NV\97!!!!!I!!!!!!!!!!!!!!!Q!!!!"%&amp;O:(E!!!!%17ZE?1!!!!2"&lt;G2Z!!!!"%&amp;O:(E!!!!%17ZE?1!!!!2"&lt;G2Z!!!!+!!!!!)!!!5!!!!!!A!!!!!!!!!%!9Q%01+6"=-!!!!!!!!!!!!!!!5!!!%:!!!"Z(C=D5^.3].!%(X.JGU;EVCV@BW52;%($S*Y]"I2B.[+"='&lt;=&lt;/2Q',+\K&lt;IL8`5(_%PU-EW[E&amp;"Z]%Q]W:Y]Q&lt;!,M\3/:,OZ-F9$?RD#9I/@*&gt;&gt;R,?4W388UF3V&amp;B*X?(N^%==U9.&amp;-[D*4JWKBSI@EKR%K-S:M7W%64N*J)X:QJ7JDJ?:6Q&gt;U/H_NSE6H*]]RG[")1)C@B08D5M,&lt;&amp;/&gt;E:J(WQ1DUC3*?MOL%L1F9&amp;BK4/U%-@!;PT!E@YYS&amp;%TE`)\(./&amp;[:U&lt;)!VD0`JUA/F8J.I/]!^#@KY*BG/"/MU&lt;D!E\B.?CZ`-^W4$9@6O"ZN5#44KA4._C)NX#PC`PJ.1R=B/B"B&lt;'"%TQD&lt;6-8;!$X,82^I!!!!!!!"X!!%!!A!$!!5!!!"9!!]%!!!!!!]!W1$5!!!!91!0"!!!!!!0!.E!V!!!!'I!$Q1!!!!!$Q$:!.1!!!"TA!#%!)!!!!]!W1$5!!!!&gt;1!4B!#!!!!4!1)!_!B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!4!!5F.31QU+!!.-6E.$4%*76Q!!%J!!!!3T!!!!)!!!%H!!!!!!!!!!!!!!!#!!!!!U!!!%K!!!!#"-35*/!!!!!!!!!:"-6F.3!!!!!!!!!;236&amp;.(!!!!!!!!!&lt;B$1V.5!!!!!!!!!=R-38:J!!!!!!!!!?"$4UZ1!!!!!!!!!@2544AQ!!!!!!!!!AB%2E24!!!!!!!!!BR-372T!!!!!!!!!D"735.%!!!!!!!!!E2W:8*T!!!!"!!!!FB41V.3!!!!!!!!!LR(1V"3!!!!!!!!!N"*1U^/!!!!!!!!!O2J9WQY!!!!!!!!!PB-37:Q!!!!!!!!!QR'5%6Y!!!!!!!!!S"46&amp;)A!!!!!!!!!T2'5%BC!!!!!!!!!UB'5&amp;.&amp;!!!!!!!!!VR75%21!!!!!!!!!X"-37*E!!!!!!!!!Y2#2%6Y!!!!!!!!!ZB#2%BC!!!!!!!!![R#2&amp;.&amp;!!!!!!!!!]"73624!!!!!!!!!^2%6%B1!!!!!!!!!_B.65F%!!!!!!!!!`R)1EF/!!!!!!!!""")1F6'!!!!!!!!"#2)36.5!!!!!!!!"$B71V21!!!!!!!!"%R'6%&amp;#!!!!!!!!"'!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!!0````]!!!!!!!!!S!!!!!!!!!!!`````Q!!!!!!!!$=!!!!!!!!!!$`````!!!!!!!!!/1!!!!!!!!!!0````]!!!!!!!!"%!!!!!!!!!!!`````Q!!!!!!!!%9!!!!!!!!!!$`````!!!!!!!!!51!!!!!!!!!!0````]!!!!!!!!"E!!!!!!!!!!!`````Q!!!!!!!!'A!!!!!!!!!!4`````!!!!!!!!!RQ!!!!!!!!!"`````]!!!!!!!!$-!!!!!!!!!!)`````Q!!!!!!!!.!!!!!!!!!!!H`````!!!!!!!!!V1!!!!!!!!!#P````]!!!!!!!!$:!!!!!!!!!!!`````Q!!!!!!!!.Y!!!!!!!!!!$`````!!!!!!!!!Z!!!!!!!!!!!0````]!!!!!!!!$J!!!!!!!!!!!`````Q!!!!!!!!1I!!!!!!!!!!$`````!!!!!!!!##Q!!!!!!!!!!0````]!!!!!!!!)0!!!!!!!!!!#`````Q!!!!!!!!B%!!!!!!!!!!$`````!!!!!!!!#01!!!!!!!!!!0````]!!!!!!!!-E!!!!!!!!!!!`````Q!!!!!!!!S9!!!!!!!!!!$`````!!!!!!!!$+!!!!!!!!!!!0````]!!!!!!!!-M!!!!!!!!!!!`````Q!!!!!!!!SY!!!!!!!!!!$`````!!!!!!!!$3!!!!!!!!!!!0````]!!!!!!!!.+!!!!!!!!!!!`````Q!!!!!!!!`A!!!!!!!!!!$`````!!!!!!!!$_A!!!!!!!!!!0````]!!!!!!!!0]!!!!!!!!!!!`````Q!!!!!!!""U!!!!!!!!!!$`````!!!!!!!!%+A!!!!!!!!!!0````]!!!!!!!!1V!!!!!!!!!#!`````Q!!!!!!!"(U!!!!!!J4:8*J97QO9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AR4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!%!!%!!!!!!!!"!!!!!1!71&amp;!!!!Z4:8*J97QO&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!:!)!!!!!!!!!!!!!!!!!!!1!!!!!!!1%!!!!$!$"!=!!/"5FO=X2S!!!:!)!!!!!!!1!%!!!!!1!!!!!!!!V736.")(*F=W^V=G.F!".!!Q!-6'FN:7^V&gt;#!I&lt;8-J!!"B!0(&gt;?6XI!!!!!R*6&lt;H2J&gt;'RF:#"-;7*S98*Z)$%/5W6S;7&amp;M,GRW9WRB=X-+5W6S;7&amp;M,G.U&lt;!!M1&amp;!!!A!!!!%&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!#!!!!!P``````````!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!:!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!$!$"!=!!/"5FO=X2S!!!:!)!!!!!!!1!%!!!!!1!!!!!!!!V736.")(*F=W^V=G.F!".!!Q!-6'FN:7^V&gt;#!I&lt;8-J!!"B!0(&gt;?6XI!!!!!R*6&lt;H2J&gt;'RF:#"-;7*S98*Z)$%/5W6S;7&amp;M,GRW9WRB=X-+5W6S;7&amp;M,G.U&lt;!!M1&amp;!!!A!!!!%&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!#!!!!!@````Y!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"E!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!)!-%"Q!!Y&amp;37ZT&gt;()!!"E!A!!!!!!"!!1!!!!"!!!!!!!!$6:*5U%A=G6T&lt;X6S9W5!71$RX8FD)Q!!!!--5W6S;7&amp;M,GRW&lt;'FC$F.F=GFB&lt;#ZM&gt;G.M98.T#F.F=GFB&lt;#ZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"E!A!!!!!!!!!!!!!!"!!!!)66O&gt;'FU&lt;'6E)%RJ9H*B=HEA-4J4:8*J97QO&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="Serial.ctl" Type="Class Private Data" URL="Serial.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="VISA resource" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">VISA resource</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">VISA resource</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Get VISA resource.vi" Type="VI" URL="../Get VISA resource.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!/"5FO=X2S!!!:!)!!!!!!!1!%!!!!!1!!!!!!!!V736.")(*F=W^V=G.F!$*!=!!?!!!&gt;$&amp;.F=GFB&lt;#ZM&gt;GRJ9AZ4:8*J97QO&lt;(:D&lt;'&amp;T=Q!+5W6S;7&amp;M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Q1(!!(A!!(1R4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!#6.F=GFB&lt;#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Set VISA resource.vi" Type="VI" URL="../Set VISA resource.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!&gt;$&amp;.F=GFB&lt;#ZM&gt;GRJ9AZ4:8*J97QO&lt;(:D&lt;'&amp;T=Q!+5W6S;7&amp;M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Q1(!!$A6*&lt;H.U=A!!'1#!!!!!!!%!"!!!!!%!!!!!!!!.6EF413"S:8.P&gt;8*D:1!Q1(!!(A!!(1R4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!#6.F=GFB&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="AddPortToReservedList.vi" Type="VI" URL="../AddPortToReservedList.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;K!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$E!B#6*F=W6S&gt;G6E0Q!;1$$`````%6:*5U%A=G6T&lt;X6S9W5A&lt;X6U!$*!=!!?!!!&gt;$&amp;.F=GFB&lt;#ZM&gt;GRJ9AZ4:8*J97QO&lt;(:D&lt;'&amp;T=Q!+5W6S;7&amp;M)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E!Q`````QV736.")(*F=W^V=G.F!$"!=!!?!!!&gt;$&amp;.F=GFB&lt;#ZM&gt;GRJ9AZ4:8*J97QO&lt;(:D&lt;'&amp;T=Q!*5W6S;7&amp;M)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!1I!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Close Serial Port.vi" Type="VI" URL="../Close Serial Port.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!-0````]26EF413"S:8.P&gt;8*D:3"P&gt;81!-E"Q!"Y!!"U-5W6S;7&amp;M,GRW&lt;'FC$F.F=GFB&lt;#ZM&gt;G.M98.T!!J4:8*J97QA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$"!=!!?!!!&gt;$&amp;.F=GFB&lt;#ZM&gt;GRJ9AZ4:8*J97QO&lt;(:D&lt;'&amp;T=Q!*5W6S;7&amp;M)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!#1!!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="Configure Serial Port.vi" Type="VI" URL="../Configure Serial Port.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)X!!!!%1!Q1(!!(A!!(1R4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!#6.F=GFB&lt;#"J&lt;A!61!=!$X2J&lt;76P&gt;81A+$%Q=W6D+1!J1!5!)X2F=GVJ&lt;G&amp;U;7^O)'.I98)++$"Y13!^)#&gt;=&lt;C=A03"-2CEA!#"!)2N&amp;&lt;G&amp;C&lt;'5A6'6S&lt;7FO982J&lt;WY+1WBB=C!I6#E!-E"Q!"Y!!"U-5W6S;7&amp;M,GRW&lt;'FC$F.F=GFB&lt;#ZM&gt;G.M98.T!!J4:8*J97QA&lt;X6U!!!81!=!%'*B&gt;71A=G&amp;U:3!I/49Q-#E!!!1!!!!41!9!$72B&gt;'%A9GFU=S!I/#E!-5!7!!5%4G^O:1.0:'1%28:F&lt;A2.98*L"6.Q97.F!!!0='&amp;S;82Z)#AQ/GZP&lt;G5J!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!E!#A!,%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'U!'!"6T&gt;'^Q)'*J&gt;(-A+$%Q/C!R)'*J&gt;#E!'U!'!"6G&lt;'^X)'.P&lt;H2S&lt;WQA+$![&lt;G^O:3E!&amp;E"1!!-!#1!+!!M*:8*S&lt;X)A&lt;X6U!(U!]!!1!!!!!1!#!!-!"!!&amp;!!9!"Q!'!!A!"A!-!!U!$A!'!!]$!!%)!!!1!!!!#!!!!!A!!!!)!!!!$1!!!!A!!!!!!!!!#!!!!!!!!!!)!!!!!!!!!!I!!!!)!!!!#!!!!!!!!!!.#Q!2!!!!!!%!!!!!!!!!!!!!!!!!!1!1!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="DeletePortFromReservedList.vi" Type="VI" URL="../DeletePortFromReservedList.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!-0````]26EF413"S:8.P&gt;8*D:3"P&gt;81!-E"Q!"Y!!"U-5W6S;7&amp;M,GRW&lt;'FC$F.F=GFB&lt;#ZM&gt;G.M98.T!!J4:8*J97QA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````].6EF413"S:8.P&gt;8*D:1!Q1(!!(A!!(1R4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!#6.F=GFB&lt;#"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!%+!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Get VISA resource as string.vi" Type="VI" URL="../Get VISA resource as string.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].6EF413"S:8.P&gt;8*D:1!S1(!!(A!!(1R4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!#F.F=GFB&lt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!-%"Q!"Y!!"U-5W6S;7&amp;M,GRW&lt;'FC$F.F=GFB&lt;#ZM&gt;G.M98.T!!F4:8*J97QA;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="ListCOMPorts.vi" Type="VI" URL="../ListCOMPorts.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;C!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].6EF413"S:8.P&gt;8*D:1!A1%!!!@````]!"2."&gt;G&amp;J&lt;'&amp;C&lt;'5A1U^.)&amp;"P=H2T!$*!=!!?!!!&gt;$&amp;.F=GFB&lt;#ZM&gt;GRJ9AZ4:8*J97QO&lt;(:D&lt;'&amp;T=Q!+5W6S;7&amp;M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Q1(!!(A!!(1R4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!#6.F=GFB&lt;#"J&lt;A"B!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!#!!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="Serial Open.vi" Type="VI" URL="../Serial Open.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$E!B#6*F=W6S&gt;G6E0Q!%!!!!-E"Q!"Y!!"U-5W6S;7&amp;M,GRW&lt;'FC$F.F=GFB&lt;#ZM&gt;G.M98.T!!J4:8*J97QA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````].6EF413"S:8.P&gt;8*D:1!Q1(!!(A!!(1R4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!#6.F=GFB&lt;#"J&lt;A"B!0!!$!!$!!1!"1!'!!5!"1!&amp;!!5!"Q!&amp;!!A!#1-!!(A!!!U)!!!*!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!%+!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="Serial Read.vi" Type="VI" URL="../Serial Read.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%U!(!!RS:82V=GYA9W^V&lt;H1!!"2!-0````],=G6B:#"C&gt;7:G:8)!-E"Q!"Y!!"U-5W6S;7&amp;M,GRW&lt;'FC$F.F=GFB&lt;#ZM&gt;G.M98.T!!J4:8*J97QA&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!=!#G*Z&gt;'5A9W^V&lt;H1!!$"!=!!?!!!&gt;$&amp;.F=GFB&lt;#ZM&gt;GRJ9AZ4:8*J97QO&lt;(:D&lt;'&amp;T=Q!*5W6S;7&amp;M)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Serial Write.vi" Type="VI" URL="../Serial Write.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!"Q!-=G6U&gt;8*O)'.P&gt;7ZU!!!S1(!!(A!!(1R4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!#F.F=GFB&lt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E!Q`````QRX=GFU:3"C&gt;7:G:8)!!$"!=!!?!!!&gt;$&amp;.F=GFB&lt;#ZM&gt;GRJ9AZ4:8*J97QO&lt;(:D&lt;'&amp;T=Q!*5W6S;7&amp;M)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="TestCOMPorts.vi" Type="VI" URL="../TestCOMPorts.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!-(!!!!(!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!/E"Q!!Y&amp;37ZT&gt;()!!"E!A!!!!!!"!!1!!!!"!!!!!!!!&amp;F:*5U%A=G6T&lt;X6S9W5A&lt;G&amp;N:3"P&gt;81!!"2!-0````],=G6B:#"C&gt;7:G:8)!%U!(!!RS:82V=GYA9W^V&lt;H1!!!Q!5!!$!!1!"1!'!"R!1!!"`````Q!($F"P=H2T)'FO)%6S=G^S!!!91%!!!@````]!"QN1&lt;X*U=S"'&lt;X6O:!!S1(!!(A!!(1R4:8*J97QO&lt;(:M;7)/5W6S;7&amp;M,GRW9WRB=X-!#F.F=GFB&lt;#"P&gt;81!!".!"Q!.1HFU:8-A&gt;']A=G6B:!!%!!!!&amp;%!Q`````QN5:8.U)(.U=GFO:Q!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!-6'FN:7^V&gt;#!I&lt;8-J!!!&lt;1!5!&amp;62F=GVJ&lt;G&amp;U;7^O)%.I98*B9X2F=A!71#%127ZB9GRF)&amp;2F=GUA1WBB=A!!$U!(!!F#986E)&amp;*B&gt;'5!%U!'!!VE982B)'*J&gt;(-A+$AJ!$&amp;!&amp;A!&amp;"%ZP&lt;G5$4W2E"%6W:7Y%47&amp;S;Q64='&amp;D:1!!$X"B=GFU?3!I-$JO&lt;WZF+1!&lt;1!9!&amp;8.U&lt;X!A9GFU=S!I-4![)$%A9GFU+1!&lt;1!9!&amp;7:M&lt;X=A9W^O&gt;(*P&lt;#!I-$JO&lt;WZF+1!21!=!#G*Z&gt;'5A9W^V&lt;H1!!#B!5!!*!!]!%!!2!")!%Q!5!"5!&amp;A!8$F.F=GFB&lt;#"0=(2J&lt;WZT!!!;1%!!!@````]!"!V736.")(*F=W^V=G.F!$"!=!!?!!!&gt;$&amp;.F=GFB&lt;#ZM&gt;GRJ9AZ4:8*J97QO&lt;(:D&lt;'&amp;T=Q!*5W6S;7&amp;M)'FO!&amp;1!]!!-!!-!#!!*!!I!#Q!-!!U!$!!/!"A!'1!;!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!A!!!!!!!!!#!!!!!!!!!!+!!!!#!!!!AA!!!#3!!!!!!%!'Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
</LVClass>
