﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Globals" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Global:EnableTimestamp" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">9</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Globals.ctl</Property>
		<Property Name="typedefName2" Type="Str">Globals_Config.ctl</Property>
		<Property Name="typedefName3" Type="Str">Globals_Demographics.ctl</Property>
		<Property Name="typedefName4" Type="Str">Globals_Indicator.ctl</Property>
		<Property Name="typedefName5" Type="Str">Globals_IO.ctl</Property>
		<Property Name="typedefName6" Type="Str">Globals_IRDriver.ctl</Property>
		<Property Name="typedefName7" Type="Str">Globals_Logging.ctl</Property>
		<Property Name="typedefName8" Type="Str">Globals_State.ctl</Property>
		<Property Name="typedefName9" Type="Str">Globals_System.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../LOGGER.exe/Utility/Variables/Globals.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">../../LOGGER.exe/Utility/Variables/Globals_Config.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">../../LOGGER.exe/Utility/Variables/Globals_Demographics.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">../../LOGGER.exe/Utility/Variables/Globals_Indicator.ctl</Property>
		<Property Name="typedefPath5" Type="PathRel">../../LOGGER.exe/Utility/Variables/Globals_IO.ctl</Property>
		<Property Name="typedefPath6" Type="PathRel">../../LOGGER.exe/Utility/Variables/Globals_IRDriver.ctl</Property>
		<Property Name="typedefPath7" Type="PathRel">../../LOGGER.exe/Utility/Variables/Globals_Logging.ctl</Property>
		<Property Name="typedefPath8" Type="PathRel">../../LOGGER.exe/Utility/Variables/Globals_State.ctl</Property>
		<Property Name="typedefPath9" Type="PathRel">../../LOGGER.exe/Utility/Variables/Globals_System.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!IF)1I!!"E!A!!!!!"F!"2!-0````],5G6N&lt;X2F)%F18T%!&amp;%!Q`````QN3:7VP&gt;'5A36"@-A!?1$$`````&amp;%:J&lt;'6O97VF)%:P=GVB&gt;#"4='6D!!!D1!)!(%2F:G&amp;V&lt;(1A5X2S:7&amp;N)%*V:G:F=C"-:7ZH&gt;'A!!"*!-P````]*9G&amp;T:3"Q982I!$=!]&gt;S)&lt;'%!!!!"%E&gt;M&lt;W*B&lt;(.@1W^O:GFH,G.U&lt;!!=1&amp;!!"1!!!!%!!A!$!!1'1W^O:GFH!!!/1$$`````"&amp;"P=H1!!"2!-0````],5G6T:8*W:71A1HE!$%"5!!9%6'FN:1!!'E"1!!-!"A!(!!A.5G6T:8*W:71A5'^S&gt;!!A1%!!!@````]!#2*3:8.F=H:F:#"$4UUA='^S&gt;(-!!#=!]&gt;SN7I5!!!!"$E&gt;M&lt;W*B&lt;(.@35]O9X2M!""!5!!"!!I$33^0!""!-0````]'37ZG97ZU!!!41!9!$8"B&gt;&amp;^E&lt;7&gt;@=X2B&gt;'5!$U!'!!BQ982@&gt;(FQ:1!!&amp;5!'!!ZQ982@='&amp;D:72@&lt;7^E:1!!&amp;%!Q`````QJH;8:F&lt;F^O97VF!!!51$$`````#W:B&lt;7FM?6^O97VF!""!-0````]'='&amp;U8WFE!!!.1!9!"X"B&gt;&amp;^T:8A!$5!&amp;!!&gt;D:7ZU&gt;8*Z!!N!"1!%?76B=A!!#U!&amp;!!6N&lt;WZU;!!*1!5!!W2B?1!,1!5!"'BP&gt;8)!!!V!"1!'&lt;7FO&gt;82F!!!.1!5!"H.F9W^O:!!!$U!&amp;!!BG=G&amp;D&gt;'FP&lt;A!!(E"1!!A!&amp;!!6!"9!&amp;Q!9!"E!'A!&lt;"X"B&gt;&amp;^E&lt;W)!#U!'!!6V&lt;GFU=Q!31$$`````#86O;82T8X.U=A!,1!I!"8:B&lt;(6F!"B!5!!$!"U!(A!@#H"B&gt;&amp;^I:7FH;(1!!"B!5!!$!"U!(A!@#H"B&gt;&amp;^X:7FH;(1!!"2!5!!$!"U!(A!@"X"B&gt;&amp;^B:W5!-E"1!!M!$1!/!!]!%!!2!")!%Q!=!#!!)1!C&amp;&amp;"I;7RJ=(-A2'6N&lt;W&gt;S98"I;7.T!!!^!0(=C'KB!!!!!2B(&lt;'^C97RT8U2F&lt;7^H=G&amp;Q;'FD=SZD&gt;'Q!(%"1!!)!$!!D$%2F&lt;7^H=G&amp;Q;'FD=Q!!'E!Q`````R"4&lt;W:U&gt;W&amp;S:3"7:8*T;7^O!!!91$$`````$F.D;'6N93"W:8*T;7^O!!!71$$`````$&amp;*V&lt;H2J&lt;75A47^E:1!!%E!Q`````QB)&lt;X.U&lt;G&amp;N:1!!%%!Q`````Q:T?8.@;71!!":!-0````]-&lt;7&amp;O&gt;7:B9X2V=G6S!!!71$$`````$'VP:'6M8WZV&lt;7*F=A!!&amp;%"1!!)!+A!L#8.Z=V^N&lt;W2F&lt;!!21!=!#WZP&lt;6^W:8*T;7^O!"N!"Q!6&gt;'6Y&gt;&amp;^D982B&lt;'^H8X*F&gt;GFT;7^O!!^!"A!)&lt;'&amp;O:X6B:W5!!!V!"A!':G^S&lt;7&amp;U!!!71&amp;!!!Q!O!#]!-!FT?8.@&lt;'^D97Q!%U!'!!RT?8.@98.T&lt;W.@;71!!"&amp;!"A!+&lt;72T8X.U982V=Q!!%E!Q`````QFC:72@&lt;'&amp;C:7Q!$5!'!!&gt;P=&amp;^N&lt;W2F!!^!"A!)98"Q8W&amp;S:7%!!!^!"A!*=X"F9V^U?8"F!".!"A!-9W^N='^O:7ZU8WFE!!!31$$`````#8"S&lt;W2@=X"F9Q!11&amp;!!!Q!X!$A!/1.W97Q!&amp;E"!!!(`````!$I)=XFT8X.Q:7-!!#J!5!!+!#E!,!!N!$%!-A!T!$1!.1!W!$M/5'BJ&lt;'FQ=S"4?8.U:7U!!!R!-0````]$1G6E!$E!]&gt;S);LI!!!!"%E&gt;M&lt;W*B&lt;(.@5XFT&gt;'6N,G.U&lt;!!?1&amp;!!"A!F!#9!*Q!I!$Q!01:4?8.U:7U!!"R!)2&gt;&amp;&lt;G&amp;C&lt;'5A4'^H:WFO:S!I2WRP9G&amp;M+1!=1#%827ZB9GRF)%RP:W&gt;J&lt;G=A4X:F=H*J:'5!&amp;E!B%56O97*M:3"1&lt;'^U&gt;'FO:S!S!#:!1!!"`````Q""'%6O97*M:3"-&lt;W&gt;H;7ZH)#B$;'&amp;O&lt;G6M+1!!"!!B!""!1!!"`````Q"$!U&amp;%1Q!31%!!!@````]!1Q61&gt;7RT:1!91%!!!@````]!1QJ1=G^Y?82S97.L!!!51%!!!@````]!1Q&gt;1;'FM;8"T!""!1!!"`````Q"$!U."41!51%!!!@````]!1Q:4?8.M&lt;W=!!#J!5!!'!%1!21"'!%=!3!"*&amp;U6O97*M:3"-&lt;W&gt;H;7ZH)#BT&gt;(*F97UJ!$9!]&gt;S);N!!!!!"%U&gt;M&lt;W*B&lt;(.@4'^H:WFO:SZD&gt;'Q!'E"1!!1!0Q"!!%)!3A&gt;-&lt;W&gt;H;7ZH!".!!A!-5XFT&gt;'6N)&amp;.U982F!!!41!)!$5ZF&gt;(&gt;P=GMA5X2B&gt;'5!'5!#!"*E:8:J9W6@97RF=H2@=X2B&gt;'5!!":!5!!"!%Y.5'BJ&lt;'FQ=S"4&gt;'&amp;U:1!&lt;1!I!&amp;%:S:75A5X"B9W5A+#5A6'^U97QJ!!!61!I!$U:S:75A5X"B9W5A+%&gt;#+1!U!0(=C'PT!!!!!2&amp;(&lt;'^C97RT8V.U982F,G.U&lt;!!;1&amp;!!"1"-!%U!4Q"1!&amp;%&amp;5X2B&gt;'5!$%!B"U."46^16V)!$5!&amp;!!&gt;-252@5&amp;&gt;.!"&amp;!"1!,4%^826*@4%F.361!%5!&amp;!!N65&amp;"&amp;5F^-35V*6!!?1&amp;!!"!"4!&amp;1!61"7$EF3)%2S;8:F=C"%162"!!!01!-!#52&amp;6F^46%&amp;521!11$$`````"V:&amp;5F.*4UY!%5!&amp;!!N-35&gt;)6&amp;^-26:&amp;4!!E1&amp;!!"Q"9!&amp;E!5Q"5!&amp;5!6A";$UF3)%2S;8:F=C"46%&amp;521!V!0(=L4`4!!!!!22(&lt;'^C97RT8UF32(*J&gt;G6S,G.U&lt;!!91&amp;!!!A"8!&amp;M*36)A2(*J&gt;G6S!!V!!A!(4%6%8V"841!31%!!!@````]!81646%&amp;521!91&amp;!!!1"?$EFO:'FD982P=C"%162"!!!21!)!#UR06U638UR*45F5!"2!1!!"`````Q"A"U*@5V2"6%5!(E"1!!1!7!":!&amp;Y!91^*&lt;G2J9W&amp;U&lt;X)A5V2"6%5!.A$RX+U`YA!!!!%62WRP9G&amp;M=V^*&lt;G2J9W&amp;U&lt;X)O9X2M!"B!5!!#!&amp;]!9AF*&lt;G2J9W&amp;U&lt;X)!.A$RX,A#7!!!!!%,2WRP9G&amp;M=SZD&gt;'Q!)E"1!!A!"1!,!#1!0A",!&amp;)!8!"D"U&gt;M&lt;W*B&lt;(-!!1"E!!!!!!!!!!!!!!!!!!"16%AQ!!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="States" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">States.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../LOGGER.exe/Utility/Variables/States.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$%Q!!!!"E!A!!!!!!)!":!-0````]-1WBB&lt;GZF&lt;#"/97VF!!!,1!-!"6.U982F!!N!!Q!&amp;5X2B:W5!'%!Q`````Q^-98.U)&amp;.Z=WRP:S".=W=!'5!$!"-D)'6M:7VF&lt;H2T)'FO)(&amp;V:86F!"*!6!!'#F2J&lt;75A5X2B&lt;8!!!")!5!!'!!!!!1!#!!-!"!!&amp;!#M!]1!!!!!!!!!"#F.U982F=SZD&gt;'Q!'%"!!!(`````!!9'5X2B&gt;'6T!!!"!!=!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
