# Overview
This is the primary data acquisition and logging software for the PANDA study.

Software is bult as an executable (LOGGER.exe) designed to be run as a service. The executables GUI (front panel) can be viewed and interacted with locally or remotely during runtime via the remote front panels interface of the VI web server (see below).

panda_logger interfaces with the following peripheral devices of the PANDA data acquisition system:
1. Graseby MR10 neonatal apnoea monitor (via USB-6001)
1. Teledyne MX300-I Oxygen Analyser (via USB-6001)
1. Fisher&Paykel Proxytrack CPAP Pressure Monitor
1. IR Webcam
1. IR LED Controller
1. Indicator PCB

The configuration of peripheral devices, data collection, logging and software control can be configured via config.INI (in application directory).

## Software Requirements
### Development environment:
LabVIEW 2019, 64-bit

LabVIEW modules/drivers:
1. NI-IMAQ, NI-IMAQdx
1. NI-DAQmx.
1. Vision Development Module

Additional packages:
1. Hidden Gems in vi.lib
1. OpenG Toolbox
1. GPower Toolbox
1. NI SysLog Library
1. Notatamelion user library (http://svn.notatamelion.com/blogProject/toolbox/Tags/Release%2019/)
1. JKI JSON

Drivers:
1. FT230X driver - https://www.ftdichip.com/Drivers/VCP.htm

### Runtime environment
All required LabVIEW software and drivers are packaged with the installer (builds\PANDA Logger Installer)

1. LabVIEW Runtime Engine
1. The same non-labview drivers as for the development environment

# Installation
## Deployment with Ansible
See labview_deploy.yml on panda_infrsatructure repo for ansible uninstall of labview, install of latest version of panda_logger from git repo, over-the-air update of peripheral device firmware, and Zabbix integration.


## Manual installation
1. Clone/pull latest panda_logger repository
    - Clone repo:

        ```bash
        git clone https://gitlab.com/panda_utas/panda_logger.git <destination path>
        ```

    - To updated an existing repo
    
        ```bash
        git reset --hard
        git clean -fxd
        git pull
        ```

1. Run the installer from `...\builds\PANDA Logger Installer\Volumepanda_logger_installer.exe` and follow the prompts. To run the installer from the commend line, run the following from an elevated powershell.
    ```ps
    & '...\Builds\PANDA Logger Installer\Volume\panda_logger_installer.exe' /q /acceptlicenses yes /r
    ```
    - The command will complete immediately but installation processes (Windows Installer) will continue in background
    - `/r` or `/r:n` suppresses restart. A manual restart may be required after installation
    - Append `/log "logfile"` to generate an install log
    - There is no stdout or stderror produced by this command
    - Default installation directory is `C:\Program Files\PANDA\`
1. Setup firewall rules silently using an admin command prompt/elevated powershell
    
    ```ps
    netsh advfirewall firewall add rule name="Inbound tcp 8000 to PANDA Logger" localport=8000 action=allow direction=in protocol=tcp  program="c:\program files\panda\logger.exe" enable=yes

    netsh advfirewall firewall add rule name="Inbound tcp 80 to PANDA Logger" localport=80 action=allow direction=in protocol=tcp  program="c:\program files\panda\logger.exe" enable=yes

    netsh advfirewall firewall add rule name="Inbound tcp 3580 to PANDA Logger" localport=3580 action=allow direction=in protocol=tcp  program="c:\program files\panda\logger.exe" enable=yes

    netsh advfirewall firewall add rule name="Inbound tcp 2343 to PANDA Logger" localport=2343 action=allow direction=in protocol=tcp  program="c:\program files\panda\logger.exe" enable=yes

    netsh advfirewall firewall add rule name="Inbound udp 2343 to PANDA Logger" localport=8000 action=allow direction=in protocol=udp  program="c:\program files\panda\logger.exe" enable=yes

    netsh advfirewall firewall add rule name="Inbound tcp 59110-59120 to PANDA Logger" localport=59110-59120 action=allow direction=in protocol=tcp  program="c:\program files\panda\logger.exe" enable=yes

    netsh advfirewall firewall add rule name="Inbound udp 6000-6010 to PANDA Logger" localport=6000-6010 action=allow direction=in protocol=tcp  program="c:\program files\panda\logger.exe" enable=yes
    ```

    - Remote debugging and remote front panels require port 8000, 80 and 3580 to be open.
    - Shared VBariables require TCP port 2343, UDP Ports 6000-6010 and a TCP port from 59110 and above for each application running.
    - See https://www.ni.com/en-au/support/documentation/supplemental/10/configuring-software-and-hardware-firewalls-to-support-national-.html for a full list of recommended ports for different situations.
    - *NI built executables have option for digital signature with custom manifest, so firewall rules could be set during installation*
1. Configure NI Licenses.

    This can be done interactively using NI License Manager, or from the command line using the command:

    ```ps
    & "C:\Program Files (x86)\National Instruments\Shared\License Manager\bin\nilmutil.exe" -s -activate "package name" -serialnumber "serialnumber"
    ```

    The following packages are currently required to be licensed:
    - NIIMAQ1394_1394_PKG 14.0000
    - Vision_VisionRuntime_PKG 19.0000  
    - Where package name is "NIIMAQ1394_1394_PKG 14.0000". ie. "<packagename> <version>"
    - FPackagename and version can be obtained from `C:\ProgramData\National Instruments\License Manager\Licenses`. There will be one file per package requiring a license.
    - Files are named `packagename_version.lic` but the version has its decimal place removed.
    - Within the .lic files the package name and version are properly represented on the third line as:
        - `PACKAGE <packagename> nilm <version> ...`
    - serialnumber is a single-seat or institutional serial number or license number.
1. Install packages from chocolatey (https://chocolatey.org/)
    - NSSM (https://nssm.cc/)
        ```ps
        choco install nssm
        ```
    - FFMPEG (https://ffmpeg.org/)
        ```ps
        choco install ffmpeg
        ```
    - mkvtoolnix (https://mkvtoolnix.download/)
        ```ps
        choco install mkvtoolnix
        ```
1. Install latest version of .NET (https://dotnet.microsoft.com/download/dotnet-framework)

1. Configure `LOGGER.exe` as a service using NSSM and start it
    ```ps
    nssm install "PANDA Logger" "C:\Program Files\PANDA\LOGGER.exe" DisplayName "PANDA Logger" Description "PANDA Logger" Start "Automatic"
    nssm start "PANDA Logger"
    ```
1. Restart the PC

# Uninstallation
1. Stop the service PANDA Logger service
    ```ps
    nssm stop "PANDA Logger"
    ```
1. Remove service listing
    ```
    nssm remove "PANDA Logger" confirm
    ```
1. Remove custom firewall rules
    ```ps
    netsh advfirewall firewall delete rule name="Inbound tcp 8000 to PANDA Logger"
    netsh advfirewall firewall delete rule name="Inbound tcp 80 to PANDA Logger"
    netsh advfirewall firewall delete rule name="Inbound tcp 3580 to PANDA Logger"
    netsh advfirewall firewall delete rule name="Inbound tcp 2343 to PANDA Logger"
    netsh advfirewall firewall delete rule name="Inbound udp 2343 to PANDA Logger"
    netsh advfirewall firewall delete rule name="Inbound tcp 59110-59120 to PANDA Logger"
    netsh advfirewall firewall delete rule name="Inbound udp 6000-6010 to PANDA Logger"
    ```
1. Uninstall the LabVIEW installation. This can be done interactively using the NI Uninstaller, or by running the command:

    ```ps
    & "C:\Program Files (x86)\National Instruments\Shared\NIUninstaller\uninst.exe" /qb /ForceDependents /x all
    ```
    - To uninstall a single package (including PANDA Logger) replace `all` with the product name ("PANDA Logger")
    - uninstallation can also generate a log by appending `/log uninstall_log.txt` to the command
    - uninstaller displays a progress bar GUI that cannot be disabled
    - **Occasionaly installation may fail with an error requiring a previous installtion/uninstallation to be undone before continuing**
1. Remove Extra Files
    - Delete the `C:\Program Files\PANDA` directory

# Software Structure
The panda_logger software is intended to be a general purpose software platform for performing data acquisition from a set of standard clinical equipment and modified equipment in general use by the GREMLINS research group. Although the panda_logger application is specifically configured for the needs of the PANDA study, it provides a set of general purpose libraries for use in similar data collection applications.

The software is based around a Class structure, with the Channel Class as the basic building block. The Class heirarchy is shown below:

![Class Hierarchy](UML/UML.svg)

The application is built up from a set of channels, each performing an asynchronous task which may be data acquisition from a specific sensor, logging or plotting of data, interfacing with/controlling a peripheral device, or performing system management tasks.

## Channel
The channel class is the basic building block of the application. Providing prototype structure for a generic asynchronous task with the following structure:

```mermaid
flowchart LR
    cr[Object Creation]-->st[Wait for Start Command]
    st-->in[Initialize]
    in-->act[Action]
    act-->act_del[Delay]
    act_del-->stp[Check for Stop]
    stp-->cls[Close]
    cls-->exit[Check for Exit]
    exit-->dsc[Discard]
    exit-->st
    stp-->act
```

This structure provides a mechanism for performing a specific Action at some interval, with the ability to start and stop performing the Action based on some internal logic or external trigger, and exit and discard based on an external trigger.

Each Channel has a Name, and may contain one or more Streams of data, each with their own Name. Data can be passed to another Channel via its Queue. And Syslog and status information can be passed to the interpreter via the Status Queue and Update Channel State.vi method. Channel execution is controlled by two notifiers (START and STOP) each with a configurable timeout. 

## Acquisition Classes
A set of classes have been created to interface with and collect data from a set of standard bedside equipment, modified equipment, and custom sensors, including the following:
- ADC - acquires data as an analogue voltage from one or more analogue input channels of a NI-USB DAQ.
- Pulse - acquires pulse counter value from one or more counter intputs of a NI-USB DAQ
- CAM - acquires image frames from any attached USB camera at a fixed sampling rate and resolution.
- Philips - acquires data from a Philips monitor with MIB Serial output, using a modified version of the VSCapture software (https://gitlab.com/panda_utas/panda_VSCapture) which is a fork of (https://sourceforge.net/p/vscapture/blog/).
- Proxytrack - acquires data from the digital output (serial) port of a Fisher&Paykel Proxytrack CPAP pressure monitor.

## Logger Classes
A set of classes that provide logging functionality in a variety of formats. The base Logger class extends the Channel class functionality to allow 

### Cration
A channel cannot be directy created, only an object of a child class.

TODO - describe the organisation of the LabVIEW code here, particularly the architecture of the monitoring tasks, consumer and producer loops, file and channel options clusters, logging, and plotting

# Data
Data is organized in the following structure

- Root folder (default is D:\PANDA)
    - \Date (YYYY-MM-DD)
        - Log file
        - \CSV
            - Data files
        - \Images
            - Video and related files

Files have three types:
## Text
These are CSV files by default but can be conigured to be any standard delimited text file.

Each CSV file has an associated metadata .JSON file.

## Log
This is used for the Syslog dump and is structured as a non-delimited .txt file.

## Video
TODO

# Config.ini
TODO - add documentation of the configuration file. Defaults, valid options for each parameter, function of each parameter.

# Remote Front Panel
The LabVIEW development environment or executable front panel/GUI can be viewed locally or remotely through a web interface from the address hostname:8000/PANDA_Logger.html.

http://www.ni.com/tutorial/4791/en/
https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z0000019LrSSAU&l=en-AU
https://zone.ni.com/reference/en-XX/help/371361R-01/lvconcepts/viewing_fp_remote/

This only works with Internet Explorer, Chrome (using the IE Tab app, or natively in versions before v42) or Firefox (untested).

## The GUI
TODO - add documentation of how to control the front panel, how it is structured and organised, and in perticular how to control the graphs.

# Remote Debug
Remote debugging is enabled on LOGGER.exe but requires the full development environment on the users machine.

https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z000000PAiTSAW&OpenDocument=&l=en-AU

# Class Documentation


# TO DO
1. Convert logfile format to SysLog. Document Facility codes and message tags.
1. Add Philips serial interface
1. Add IR LED Controller interface & complete control logic
1. Expand logging
1. Finalise valid signal ranges for each sensor & add detection/estimation of care periods or major sensor disconnections.